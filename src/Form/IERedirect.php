<?php

/**
 * @file
 * Contains Drupal\ie_redirect\Form\IERedirect.
 */

namespace Drupal\ie_redirect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class IERedirect.
 *
 * @package Drupal\ie_redirect\Form
 */
class IERedirect extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ie_redirect.config'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ie_redirect';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ie_redirect.config');
    $form['redirect_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL'),
      '#description' => $this->t('IE users will be redirected to this URL.'),
      '#default_value' => $config->get('redirect_url'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('ie_redirect.config')
      ->set('redirect_url', $form_state->getValue('redirect_url'))
      ->save();
  }

}
