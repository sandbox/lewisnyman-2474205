(function () {

  "use strict";

  var $url = drupalSettings.ie_redirect.url;

  if($url) {
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    if (isIE) {
      window.location.href = drupalSettings.ie_redirect.url;
    }
  }

})();
